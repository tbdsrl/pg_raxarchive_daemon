"""

"""
import sys
import logging
import argparse
from ConfigParser import ConfigParser

__version__ = '1.0'
__author__ = 'Saverio Mucci'


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--config', default='/etc/pg_raxarchive_deamon.ini')
    parser.add_argument('-d', '--debug', dest='loglevel', default=logging.WARNING,
                        action='store_const', const=logging.DEBUG)

    args = parser.parse_args()

    logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s',
                        level=args.loglevel,
                        handlers=[logging.StreamHandler(sys.stderr)])

    config_parser = ConfigParser({
        'pyrax_use_public': 'no',
        'workers': '1',
        'archive_compress': 'yes'
    })
    config_parser.read(args.config)
    # pyrax options
    pyrax_identity_file = config_parser.get('pg_raxarchive_deamon', 'pyrax_identity_file')
    pyrax_region = config_parser.get('pg_raxarchive_deamon', 'pyrax_region')
    pyrax_container = config_parser.get('pg_raxarchive_deamon', 'pyrax_container')
    pyrax_use_public = config_parser.getboolean('pg_raxarchive_deamon', 'pyrax_use_public')
    # deamon options
    archive_directory_glob = config_parser.get('pg_raxarchive_deamon', 'archive_directory_glob')
    archive_compress = config_parser.getboolean('pg_raxarchive_deamon', 'archive_compress')
    workers = config_parser.getint('pg_raxarchive_deamon', 'workers')

    from deamon import ArchiveDirectoryUploader
    archiver = ArchiveDirectoryUploader(pyrax_identity_file, pyrax_region, pyrax_container,
                                        archive_directory_glob, archive_compress, pyrax_use_public,
                                        workers)
    archiver.run()

if __name__ == '__main__':
    sys.exit(main())
