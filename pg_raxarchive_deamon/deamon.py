import os
import time
import glob
import logging
from concurrent.futures import ThreadPoolExecutor as Pool
# from multiprocessing import Pool

from pg_raxarchive.archiver import PGRaxArchiver

logger = logging.getLogger(__name__)


class ArchiveDirectoryUploader(object):

    def __init__(self, pyrax_identity_file, region, container_name, archive_directory_glob,
                 compress=True, use_public=False, workers=1):
        self.pyrax_identity_file = pyrax_identity_file
        self.region = region
        self.container_name = container_name
        self.use_public = use_public
        self.workers = workers
        self.archive_directory_glob = archive_directory_glob
        self.pool = Pool(self.workers)
        self.archiver = PGRaxArchiver(pyrax_identity_file, region, container_name, use_public)
        self.compress = compress

    def run(self):
        try:
            self._run()
        except KeyboardInterrupt:
            logger.warn('Exiting...')

    def _run(self):
        while(True):
            files = map(os.path.abspath, glob.glob(self.archive_directory_glob))
            for res in self.pool.map(self.handle_wal_file, files):
                pass

            # I' want to sleep in case there are no files to upload
            if not files:
                time.sleep(1)

    def handle_wal_file(self, file_path):
        logger.debug('Start uploading {}'.format(file_path))
        self.archiver.upload(src_name=file_path, dst_name=os.path.basename(file_path), compress=self.compress)
        logger.info('Upload completed {}'.format(file_path))
        os.remove(file_path)
        logger.debug('Deleted {}'.format(file_path))
