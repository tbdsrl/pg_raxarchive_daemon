WAL file archiving deamon for rackspace
=======================================

This is a simple deamon that will keep monitoring the files in a directory
and uploads and delete the new files that it founds

Most of the solution is based on https://github.com/duilio/pg_raxarchive

Installation
------------

    pip install git+https://bitbucket.org/tbdsrl/pg_raxarchive_daemon#egg=pg_raxarchive_daemon


Usage
-----

You have to create a configuration file like `pg_raxarchive_deamon.ini`:

    [pg_raxarchive_deamon]
    pyrax_identity_file: /etc/pg_raxarchive.ini
    pyrax_region: ORD
    pyrax_container: cloudfile_container_name
    archive_directory_glob: /var/lib/postgresql/9.1/main/archived_pg_xlog/*

And start the process:

    pg_raxarchive_deamon --config=pg_raxarchive_deamon.ini
