import setuptools
import pg_raxarchive_deamon

REQUIRES = [
    'pg_raxarchive>=1.1',
    'futures>=3.0.5'
]

setuptools.setup(
    name='pg_raxarchive_deamon',
    version=pg_raxarchive_deamon.__version__,
    author=pg_raxarchive_deamon.__author__,
    author_email='saver.mucci@gmail.com',
    description=pg_raxarchive_deamon.__doc__.splitlines()[0].strip(),
    long_description='\n'.join(pg_raxarchive_deamon.__doc__.splitlines()[1:]).strip(),
    url='https://bitbucket.org/tbdsrl/pg_raxarchive_daemon',
    license='BSD',

    packages=['pg_raxarchive_deamon'],
    install_requires=REQUIRES,

    entry_points={
        'console_scripts': [
            'pg_raxarchive_deamon = pg_raxarchive_deamon:main'
        ],
    },

    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Information Technology',
        'Intended Audience :: System Administrators',
        'Intended Audience :: Developers',
        'Operating System :: POSIX',
        'License :: OSI Approved :: BSD License',
        'Programming Language :: Python :: 2.7',
        'Topic :: Utilities',
        'Topic :: Software Development :: Libraries :: Python Modules'],
)
